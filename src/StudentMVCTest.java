public class StudentMVCTest {
    public static void main(String[] args) {
        Student model  = getStudentFromDatabase();

        StudentView view = new StudentView();

        StudentController controller = new StudentController(model, view);

        controller.updateView();

        controller.setStudentName("Mike","Tyson");
        controller.setStudentID(1002);

        controller.updateView();
    }
    private static Student getStudentFromDatabase()
    {
        Student student = new Student("Jack","Martin",1001);
        return student;
    }
}